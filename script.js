const gameContainer = document.getElementById("game");

const COLORS = ["red", "blue", "green", "orange", "purple"];

const GIFS = [
  "1.gif",
  "2.gif",
  "3.gif",
  "4.gif",
  "5.gif",
  "6.gif",
  "7.gif",
  "8.gif",
  "9.gif",
  "10.gif",
  "11.gif",
  "12.gif",
];
// Variables to store cards value
let cardOneClass;
let cardOneId;
let cardsClosed = true;
let totalCards;
let totalSolved = 0;
let openedCardsCount = 0;
let openedCardsIds = [];
let cardsToDisplay = 6;
// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array, limit) {
  if (limit !== "") {
    array = array.slice(0, Number(Math.ceil(limit / 2)));
  }

  array = array.concat(array);
  let counter = array.length;
  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledArrays = shuffle(GIFS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForCards(finalCards) {
  let index = 0;
  totalCards = finalCards.length;

  gameContainer.innerHTML = "";
  for (let card of finalCards) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(card.replace(".", "-"));
    newDiv.classList.add("card");

    const frontCard = document.createElement("section");
    frontCard.setAttribute("class", `card-${index}-front card-front`);
    frontCard.style.height = "100%";
    frontCard.style.width = "100%";

    const frontCardImage = document.createElement("img");
    frontCardImage.setAttribute("class", "front-img");
    frontCardImage.setAttribute("src", "/images/question-solid.svg");
    // frontCardImage.style.height = "100%";
    // frontCardImage.style.width = "100%";
    frontCard.appendChild(frontCardImage);

    const backCard = document.createElement("section");
    backCard.setAttribute("class", `card-${index}-back card-back`);

    const backCardImage = document.createElement("img");
    backCardImage.setAttribute("src", "/gifs/" + card);
    backCardImage.style.height = "100%";
    backCardImage.style.width = "100%";
    backCard.appendChild(backCardImage);
    // give id to each card
    newDiv.appendChild(frontCard);
    newDiv.appendChild(backCard);

    newDiv.id = `card-${index}`;

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
    index++;
  }
}

// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked

  let card = event.target.closest("div");
  if (!cardsClosed) {
    return;
  }
  if (!openedCardsIds.includes(card.id)) {
    card.classList.toggle("flipCard");
  } else {
    return;
  }

  const score = document.getElementById("score");
  score.textContent = Number(score.textContent) + 1;

  if (openedCardsCount === 0) {
    openedCardsCount = 1;
    cardOneId = card.id;
    cardOneClass = card.className.split(" ")[0];
  } else if (openedCardsCount === 1) {
    if (cardOneId === card.id) {
      openedCardsCount = 0;
      return;
    }

    if (cardOneClass !== card.className.split(" ")[0]) {
      cardsClosed = false;
      setTimeout(() => {
        const oldCard = document.getElementById(cardOneId);
        oldCard.classList.toggle("flipCard");

        card.classList.toggle("flipCard");
        cardsClosed = true;
      }, 1000);
      openedCardsCount = 0;
    } else {
      openedCardsIds.push(cardOneId, card.id);

      totalSolved += 2;

      if (totalSolved == totalCards) {
        let bestScore = document.getElementById("best");
        if (
          Number(score.textContent) < Number(bestScore.textContent) ||
          bestScore.textContent === "0"
        ) {
          bestScore.textContent = score.textContent;
          localStorage.setItem("best", score.textContent);
        }
        showGameOverPopup(score.textContent, bestScore.textContent);
      }

      openedCardsCount = 0;
    }
  }
}

// when the DOM loads

const startGame = document.getElementById("start-game");

startGame.addEventListener("click", () => {
  createDivsForCards(shuffle(GIFS, cardsToDisplay));
  const welcome = document.getElementById("welcome");
  welcome.style.display = "none";
  document.body.style.overflow = "visible";
});

function init() {
  openedCardsCount = 0;
  totalSolved = 0;
  openedCardsIds = [];
  const score = document.getElementById("score");
  score.textContent = "0";

  if (localStorage.getItem("best")) {
    let bestScore = document.getElementById("best");
    bestScore.textContent = localStorage.getItem("best");
  }
}

init();

function showGameOverPopup(score, bestScore) {
  gameContainer.scrollIntoView();
  let popupbg = document.createElement("div");
  popupbg.classList.add("popupbg");
  let div = document.createElement("div");
  div.classList.add("popup");
  let header = document.createElement("h1");
  header.textContent = "Game Over";

  let paragraph = document.createElement("p");
  paragraph.textContent = `Your Score: ${score}`;

  let paragraph2 = document.createElement("p");
  paragraph2.textContent = `Best Score: ${bestScore}`;

  let button = document.createElement("button");
  button.classList.add("restart-button");
  button.textContent = "Restart Game";
  button.addEventListener("click", restartGameEnclose(popupbg));
  div.appendChild(header);
  div.appendChild(paragraph);
  div.appendChild(paragraph2);
  div.appendChild(button);
  popupbg.appendChild(div);
  gameContainer.insertAdjacentElement("afterend", popupbg);
  document.body.style.overflow = "hidden";
}

function restartGameEnclose(element) {
  let requiredNode = element;
  return function () {
    requiredNode.remove();
    let game = document.getElementById("game");
    game.innerHTML = "";
    createDivsForCards(shuffle(GIFS, cardsToDisplay));
    document.body.style.overflow = "visible";
    const score = document.getElementById("score");
    score.textContent = "0";
    init();
  };
}

window.onbeforeunload = function () {
  const welcome = document.getElementById("welcome");
  welcome.style.display = "flex";
  const game = document.getElementById("game-container");
  game.style.display = "none";
};

function resetGame() {
  createDivsForCards(shuffle(GIFS, cardsToDisplay));
  init();
}

function setLevel() {
  let level = document.getElementById("difficulty");
  cardsToDisplay = level.value;
}

function resetGamePopup() {
  gameContainer.scrollIntoView();
  let popupbg = document.createElement("div");
  popupbg.classList.add("popupbg");
  let div = document.createElement("div");
  div.classList.add("popup");
  let header = document.createElement("h1");
  header.textContent = "Do you really want to reset the game?";

  let resetButton = document.createElement("button");
  resetButton.classList.add("reset-button");
  resetButton.textContent = "Reset";
  resetButton.addEventListener("click", restartGameEnclose(popupbg));
  let cancelButton = document.createElement("button");
  cancelButton.classList.add("close-button");
  cancelButton.textContent = "Close";
  cancelButton.addEventListener("click", closePopup(popupbg));
  div.appendChild(header);
  div.appendChild(resetButton);
  div.appendChild(cancelButton);
  popupbg.appendChild(div);
  gameContainer.insertAdjacentElement("afterend", popupbg);
  document.body.style.overflow = "hidden";
}

function closePopup(element) {
  return function () {
    document.body.style.overflow = "visible";
    element.remove();
  };
}
